const apiRouter = require("express").Router();
const v1 = require("./v1");

apiRouter.get("/", (req, res) => {
  res.send("test");
});

apiRouter.use("/v1", v1);

// swagger
const swaggerUI = require("swagger-ui-express");
const swaggerJSON = require("../documentation/swagger.json");

apiRouter.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON))


module.exports = apiRouter;
